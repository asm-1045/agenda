<?php
namespace controller;
use model\sistemaControle;

include_once '../model/sistemaControle.php';


  abstract class dadosAgenda {
  
  public $conexao; 
  public $codigo;
  public $empresa;
  public $telefoneCom;
  public $skipe;
  public $email;
  public $site;
  public $cidade;
  public $estado;
  public $bairro;
  public $endereco;
  public $msg;
  public $nome;
  public $celular;
  public $dataCompromisso;
  public $hora;
  public $tipocompromisso;
  public $localcompromisso;
  public $vinculocliente;
  public $descricaoMsn;
  public $usuario;
  public $senha;
  public $codValidacao;
  public $status;
  public $data;
  public $reunioes;
  
  
  public function getData() {
      return $this->data;
  }

  public function getReunioes() {
      return $this->reunioes;
  }

  public function setData($data) {
      $this->data = $data;
  }

  public function setReunioes($reunioes) {
      $this->reunioes = $reunioes;
  }

    public function getStatus() {
      return $this->status;
  }

  public function setStatus($status) {
      $this->status = $status;
  }

    public function getCodValidacao() {
      return $this->codValidacao;
  }

  public function setCodValidacao($codValidacao) {
      $this->codValidacao = $codValidacao;
  }

    
  public function getUsuario() {
      return $this->usuario;
  }

  public function getSenha() {
      return $this->senha;
  }

  public function setUsuario($usuario) {
      $this->usuario = $usuario;
  }

  public function setSenha($senha) {
      $this->senha = $senha;
  }

    public function getDataCompromisso() {
      return $this->dataCompromisso;
  }

  public function getHora() {
      return $this->hora;
  }

  public function getTipocompromisso() {
      return $this->tipocompromisso;
  }

  public function getLocalcompromisso() {
      return $this->localcompromisso;
  }

  public function getVinculocliente() {
      return $this->vinculocliente;
  }

  public function getDescricaoMsn() {
      return $this->descricaoMsn;
  }

  public function setDataCompromisso($dataCompromisso) {
      $this->dataCompromisso = $dataCompromisso;
  }

  public function setHora($hora) {
      $this->hora = $hora;
  }

  public function setTipocompromisso($tipocompromisso) {
      $this->tipocompromisso = $tipocompromisso;
  }

  public function setLocalcompromisso($localcompromisso) {
      $this->localcompromisso = $localcompromisso;
  }

  public function setVinculocliente($vinculocliente) {
      $this->vinculocliente = $vinculocliente;
  }

  public function setDescricaoMsn($descricaoMsn) {
      $this->descricaoMsn = $descricaoMsn;
  }

    
  public function getCelular() {
      return $this->celular;
  }

  public function setCelular($celular) {
      $this->celular = $celular;
  }
    
  public function getNome() {
      return $this->nome;
  }

  public function setNome($nome) {
      $this->nome = $nome;
  }
     
  public function getCodigo() {
      return $this->codigo;
  }

  public function setCodigo($codigo) {
      $this->codigo = $codigo;
  }
  
  public function getEmpresa() {
      return $this->empresa;
  }

  public function getTelefoneCom() {
      return $this->telefoneCom;
  }

  public function getSkipe() {
      return $this->skipe;
  }

  public function getEmail() {
      return $this->email;
  }

  public function getSite() {
      return $this->site;
  }

  public function getCidade() {
      return $this->cidade;
  }

  public function getEstado() {
      return $this->estado;
  }

  public function getBairro() {
      return $this->bairro;
  }

  public function getEndereco() {
      return $this->endereco;
  }

  public function getMsg() {
      return $this->msg;
  }

  public function setEmpresa($empresa) {
      $this->empresa = $empresa;
  }

  public function setTelefoneCom($telefoneCom) {
      $this->telefoneCom = $telefoneCom;
  }

  public function setSkipe($skipe) {
      $this->skipe = $skipe;
  }

  public function setEmail($email) {
      $this->email = $email;
  }

  public function setSite($site) {
      $this->site = $site;
  }

  public function setCidade($cidade) {
      $this->cidade = $cidade;
  }

  public function setEstado($estado) {
      $this->estado = $estado;
  }

  public function setBairro($bairro) {
      $this->bairro = $bairro;
  }

  public function setEndereco($endereco) {
      $this->endereco = $endereco;
  }

  public function setMsg($msg) {
      $this->msg = $msg;
  }


  abstract protected function gravarRegistro();  
  abstract protected function pesquisaRegistroCliente();
  abstract protected function inserirCompromisso();
  abstract protected function pesquisaCompromissoDia();
  abstract protected function removerRegistro();
  abstract protected function infoAgendamento();
  abstract protected function acessoAplicacao();
  abstract protected function gerarCodigo();
  abstract protected function atualizaStatus();
  abstract protected function pesquisaPeriodoReuniao();
  abstract protected function registroAgenda();
  
  }
  
 class controleDados extends dadosAgenda { 
  
     
   public function __construct(){
    
    $this->conexao = new sistemaControle();
   
   }
  
   
   public function gravarRegistro(){
   
      return $this->conexao->gravarDadosContato($this);
   }
   
   public function removerRegistro(){
   
      return $this->conexao->apagarDados($this);
   }
   
   public function pesquisaRegistroCliente(){
   
      return $this->conexao->pesquisaCliente($this);
   }
   
   public function inserirCompromisso(){
   
      return $this->conexao->gravarCompromisso($this);
   }
  
   public function pesquisaCompromissoDia(){
   
      return $this->conexao->pesquisaAgendamentoDia($this);
   }
   
   public function infoAgendamento(){
   
      return $this->conexao->pesquisaInfoAgendado($this);
   }
  
   public function acessoAplicacao(){
   
      return $this->conexao->pesquisaAcesso($this);
   }
   
   public function gerarCodigo(){
   
      return $this->conexao->gravarCodigo($this);
   }
  
   public function atualizaStatus(){
   
      return $this->conexao->atualizarStatusAgenda($this);
   }
   
   public function pesquisaPeriodoReuniao(){
   
      return $this->conexao->reuniaoPeriodo($this); 
   }
   
   public function registroAgenda(){
   
      return $this->conexao->criarAgenda($this); 
   }
  
  
  }
