<?php
namespace view;
use controller\controleDados;

include_once '../controller/controleDados.php';

if (empty ($_POST['inputUsuario'])):    
    header("location:../view/index.php?error=1");
endif;
if (empty ($_POST['inputSenha'])):    
    header("location:../view/index.php?error=1");
endif;

$pesqAcesso = new controleDados();

$pesqAcesso->setUsuario($_POST['inputUsuario']);
$pesqAcesso->setSenha($_POST['inputSenha']);

$arq = $pesqAcesso->acessoAplicacao();



if (count($arq) == 1):
  
  header("location:../view/index.php?error=2");  
else:  
   $dataAtual = date('Y-m-d H:i');
   $codBase = base64_encode($_POST['inputUsuario'].$_POST['inputSenha'].$dataAtual);
   $pesqAcesso->setCodValidacao($codBase);   
   $pesqAcesso->gerarCodigo(); 
     header("location:../view/agenda.php");  
endif;