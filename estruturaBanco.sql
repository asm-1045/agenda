-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.25-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para agenda_eletronica
CREATE DATABASE IF NOT EXISTS `agenda_eletronica` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `agenda_eletronica`;

-- Copiando estrutura para tabela agenda_eletronica.acesso
CREATE TABLE IF NOT EXISTS `acesso` (
  `acesso_id` int(11) NOT NULL AUTO_INCREMENT,
  `acesso_codusuario` varchar(50) NOT NULL,
  `acesso_data` date NOT NULL,
  `acesso_hora` time NOT NULL,
  `acesso_cod_acesso` varchar(50) NOT NULL,
  PRIMARY KEY (`acesso_id`),
  KEY `FKAcesso` (`acesso_codusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela agenda_eletronica.agenda
CREATE TABLE IF NOT EXISTS `agenda` (
  `agenda_id` int(11) NOT NULL AUTO_INCREMENT,
  `agenda_cod_agendamentos` int(11) NOT NULL,
  `agenda_status` enum('S','N','A') DEFAULT 'A' COMMENT 'S -sim | N - não | A - Aguardando',
  PRIMARY KEY (`agenda_id`),
  KEY `FKAgenda` (`agenda_cod_agendamentos`),
  CONSTRAINT `FKAgenda` FOREIGN KEY (`agenda_cod_agendamentos`) REFERENCES `agendamento` (`agendamento_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela agenda_eletronica.agendamento
CREATE TABLE IF NOT EXISTS `agendamento` (
  `agendamento_id` int(11) NOT NULL AUTO_INCREMENT,
  `agendamento_nome` varchar(50) NOT NULL,
  `agendamento_descricao` text,
  `agendamento_data` date NOT NULL,
  `agendamento_titulo` varchar(50) NOT NULL,
  `agendamento_hora` time NOT NULL,
  `agendamento_local` varchar(50) NOT NULL,
  PRIMARY KEY (`agendamento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela agenda_eletronica.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_nome` varchar(50) NOT NULL,
  `cliente_empresa` varchar(50) NOT NULL,
  `cliente_data` date NOT NULL,
  `cliente_telefone` char(11) DEFAULT NULL,
  `cliente_celular` char(11) DEFAULT NULL,
  `cliente_skype` varchar(50) DEFAULT NULL,
  `cliente_estado` char(2) DEFAULT NULL,
  `cliente_cidade` varchar(50) DEFAULT NULL,
  `cliente_bairro` varchar(50) DEFAULT NULL,
  `cliente_endereco` varchar(50) DEFAULT NULL,
  `cliente_email` varchar(50) DEFAULT NULL,
  `cliente_site` varchar(50) DEFAULT NULL,
  `cliente_info` text,
  PRIMARY KEY (`cliente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
-- Copiando estrutura para tabela agenda_eletronica.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_usuario` varchar(50) NOT NULL,
  `usuario_senha` varchar(50) NOT NULL,
  `usuario_data` date NOT NULL,
  PRIMARY KEY (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
