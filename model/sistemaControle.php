<?php
namespace model;
use model\conexao;

include_once 'conexao.php';

   class sistemaControle{
   
     public $conn;
     
     public function __construct(){
     
      $this->conn = new conexao();
     
     }
   
     
     public function pesquisaCliente($arq){

       $nomePesq = $arq->getNome();
         
       $search = $nomePesq.'%';
       
       $sql = "SELECT cliente_nome FROM cliente WHERE cliente_nome LIKE :nome";
       $query = $this->conn->pdo->prepare($sql);
       $query->bindParam(':nome', $search);
       
       $query->execute();
       
         $row = $query->fetchAll();
         
         return $row;
     
     }
     
     public function pesquisaAgendamentoDia($arq){

       $dataPesq = $arq->getDataCompromisso();
                       
       $sql = "SELECT ageNo.agendamento_nome, ageNo.agendamento_descricao, ageNo.agendamento_hora, ageNo.agendamento_titulo, age.agenda_cod_agendamentos, age.agenda_status
               FROM agenda age INNER JOIN agendamento ageNo on age.agenda_cod_agendamentos = ageNo.agendamento_id WHERE 
               ageNo.agendamento_data = ?";       
       
       $query = $this->conn->pdo->prepare($sql);
       $query->bindParam(1, $dataPesq);
       
       $query->execute();
       
         $row = $query->fetchAll();
         
         return $row;
     
     }
     
     public function pesquisaAcesso($arq){

       $usuario = $arq->getUsuario();
       $senha = $arq->getSenha();
                       
       $sql = "SELECT usuario_usuario FROM usuario WHERE usuario_usuario = ? AND usuario_senha = ?";       
       
       $query = $this->conn->pdo->prepare($sql);
       $query->bindParam(1, $usuario);
       $query->bindParam(2, $senha);
       
       $query->execute();
       
         return $query->fetch();
              
     }
     
     public function reuniaoPeriodo($arq){

       $dataFim = $arq->getData();
       $dataAtual = date("Y-m-01");
                       
              
       $sql = "SELECT agendamento_data, count(agendamento_data) as somadias FROM agendamento WHERE agendamento_data BETWEEN ? AND ?
               GROUP BY agendamento_data";       
       
       $query = $this->conn->pdo->prepare($sql);
       $query->bindParam(1, $dataAtual);
       $query->bindParam(2, $dataFim);
       
       $query->execute();
       
         return $query->fetchAll();
              
     }
     
     public function pesquisaInfoAgendado($arq){

       $nomePesq = $arq->getNome();
         
       
       $sql = "SELECT ageNo.agendamento_nome, ageNo.agendamento_descricao,DATE_FORMAT (ageNo.agendamento_data, '%d/%m/%Y') as data_agenda, ageNo.agendamento_hora, ageNo.agendamento_titulo,
                cli.cliente_empresa, cli.cliente_telefone, cli.cliente_celular, cli.cliente_skype, cli.cliente_estado,
                cli.cliente_cidade, cli.cliente_bairro, cli.cliente_endereco,cli.cliente_email,cli.cliente_site
                FROM agenda age INNER JOIN agendamento ageNo on age.agenda_cod_agendamentos = ageNo.agendamento_id 
                INNER JOIN cliente cli on ageNo.agendamento_nome = cli.cliente_nome
                WHERE ageNo.agendamento_nome = ?";
       
       $query = $this->conn->pdo->prepare($sql);
       $query->bindParam(1, $nomePesq);
       
       $query->execute();
       
         $row = $query->fetchAll();
         
         return $row;
     
     }
     
     
     public function gravarDadosContato($arq){
         
        $nome = $arq->getNome();
        $empresa = $arq->getEmpresa();
        $telComer = $arq->getTelefoneCom();
        $skipe = $arq->getSkipe();
        $email = $arq->getEmail();
        $site = $arq->getSite();
        $cidade = $arq->getCidade();
        $estado = $arq->getEstado();
        $bairro = $arq->getBairro();
        $endereco = $arq->getEndereco();
        $msg = $arq->getMsg();  
        $celular = $arq->getCelular();  
        
         $sql = "INSERT INTO cliente (cliente_nome,cliente_empresa,cliente_telefone,cliente_celular,cliente_skype,
                 cliente_estado,cliente_cidade,cliente_bairro,cliente_endereco,cliente_email,cliente_site,cliente_info,cliente_data) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,NOW())";
                  
         $query = $this->conn->pdo->prepare($sql);        
                  
         $query->bindParam(1,$nome);
         $query->bindParam(2,$empresa);
         $query->bindParam(3,$telComer);
         $query->bindParam(4,$celular);
         $query->bindParam(5,$skipe);
         $query->bindParam(6,$estado);
         $query->bindParam(7,$cidade);
         $query->bindParam(8,$bairro);
         $query->bindParam(9,$endereco);         
         $query->bindParam(10,$email);
         $query->bindParam(11,$site);
         $query->bindParam(12,$msg);
         
         $query->execute();
              
         
     }
     
     public function gravarCompromisso($arq){
               
       
        $dataCompromisso = $arq->getDataCompromisso();
        $hora = $arq->getHora();
        $compromisso = $arq->getTipocompromisso();
        $localCompromisso = $arq->getLocalcompromisso();
        $cliente = $arq->getVinculocliente();
        $msn = $arq->getDescricaoMsn();
        
         $sql = "INSERT INTO agendamento (agendamento_nome,agendamento_descricao,agendamento_titulo,agendamento_local,agendamento_data,agendamento_hora) VALUES (?,?,?,?,?,?)";
                  
         $query = $this->conn->pdo->prepare($sql);        
                  
         $query->bindParam(1,$cliente);
         $query->bindParam(2,$msn);
         $query->bindParam(3,$compromisso);         
         $query->bindParam(4,$localCompromisso);         
         $query->bindParam(5,$dataCompromisso);
         $query->bindParam(6,$hora);
         
         $query->execute();
         var_dump($query->errorInfo());
         
         print $last_id = $this->conn->pdo->lastInsertId();
         
         return $last_id;
     }
     
     
     
     public function criarAgenda($arq){
              
       
        $codigo = $arq->getCodigo();
        $status = 'A';
        
         $sql = "INSERT INTO agenda(agenda_cod_agendamentos,agenda_status) VALUES (?,?)";
                  
         $query = $this->conn->pdo->prepare($sql);        
                  
         $query->bindParam(1,$codigo);         
         $query->bindParam(2,$status);         
         
         $query->execute();
         
     }
     
     
     public function gravarCodigo($arq){
               
       
        $codBase = $arq->getCodValidacao();
        $codUser = $arq->getUsuario();
       
        
         $sql = "INSERT INTO acesso (acesso_codusuario, acesso_cod_acesso, acesso_data,acesso_hora) VALUES (?,?,NOW(),NOW())";
                  
         $query = $this->conn->pdo->prepare($sql);        
                  
         $query->bindParam(1,$codUser);
         $query->bindParam(2,$codBase);         
         
         $query->execute();
             
     }
     
     public function inserirAgenda($arq){
         
        $cod = $arq->getCodigo();
        
         $sql = "INSERT INTO agenda(agenda_cod_agendamento, agenda_status) VALUES (?,?)";
                  
         $query = $this->conn->pdo->prepare($sql);        
                  
         $query->bindParam(1,$cod);         
         $query->bindParam(2,'S');         
         
         $query->execute();
           
     }
     
     public function apagarDados($arq){
        
        $nome = $arq->getNome();
        
         $sql = "DELETE FROM agendamento WHERE agendamento_nome = ?";
                  
         $query = $this->conn->pdo->prepare($sql);        
                  
         $query->bindParam(1,$nome);
                  
         $query->execute();
                  
     }
     
     public function atualizarStatusAgenda($arq){
        
        $codSeq = $arq->getNome();
        $status = $arq->getStatus();
        
        
         $sql = "UPDATE agenda SET agenda_status = ? WHERE agenda_cod_agendamentos = ?";
                  
         $query = $this->conn->pdo->prepare($sql);        
                  
         $query->bindParam(1,$status);
         $query->bindParam(2,$codSeq);
                  
         $query->execute();
         
                          
     }
     
   
   }

