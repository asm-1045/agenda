<?php
namespace view;
use controller\controleDados;

include_once '../controller/controleDados.php';

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>Agenda Eletrônica</title>    
  <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
  <style type="text/css">

    *{
        font-family: calibri;
    }

    .box {
        margin: 0px auto;
        width: 70%;
    }

    .box-chart {
        width: 120%;
        margin: 0 auto;
        padding: 10px;
    }
    

    </style>
    <script type="text/javascript">
        var randomnb = function(){ return Math.round(Math.random()*300)};
    </script>
 
</head>
<body>
   
<div class="jumbotron">
      <div class="container">
         <h1>Agenda Telefônica</h1>         
          <a class="btn btn-primary" href="agenda.php" role="button">Home &raquo;</a>
          <a class="btn btn-primary" href="administrativo.php" role="button">Administração &raquo;</a>
      </div>
    
</div>
    
<div class="jumbotron">
    <div class="container">
      <h2>:: Gráfico ::</h2>

      
<div class="box">
        
            <canvas id="GraficoBarra" style="width:40%;"></canvas>
            
<?php

$dadosGraficos = new controleDados();

$dadosGraficos->setData(date("Y-m-t"));
$dadosPeriodo = $dadosGraficos->pesquisaPeriodoReuniao(); 

        foreach ($dadosPeriodo as $infoValue) {
            $data[] = $infoValue['agendamento_data'];
            $quantDias[] = $infoValue['somadias'];
        }        
        
?>

<script type="text/javascript">

                var options = {
                    responsive:true
                };

                var data = {
                    labels: <?=json_encode(array_values($data));?>,
                    datasets: [
                        {
                            label: "Dados primÃ¡rios",
                            fillColor: "rgba(151,187,205,0.2)",
                            strokeColor: "rgba(151,187,205,0.2)",
                            pointColor: "rgba(151,187,205,0.2)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(151,187,205,0.2)",
                            data : <?=json_encode(array_values($quantDias));?>
                        },

                    ]
                };

                window.onload = function(){

                    var ctx = document.getElementById("GraficoBarra").getContext("2d");
                    var LineChart = new Chart(ctx).Bar(data, options);
                }
            </script>   
            <h3>Reuniões por dia</h3>
            </div>
    </div>
</div>
  
    
</body>
</html>

  <script src="./js/jquery.js"></script>
  <script src="./bootstrap/js/bootstrap.min.js"></script>
 
<script src="./js/Chart.min.js"></script>
    
