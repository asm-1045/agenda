<?php
namespace view;
use controller\controleDados;

include_once '../controller/controleDados.php';
date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>Agenda Eletrônica</title>    
  <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
</head>
<body>
   
<div class="jumbotron">
      <div class="container">
         <h1>Agenda Telefônica</h1>         
         <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAgendar"> Agendamentos &raquo;</button> 
         <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalContato"> Adicionar Contato &raquo;</button>                 
         <a class="btn btn-primary " href="pesquisa.php" role="button">Pesquisa Agenda &raquo;</a>         
         <a class="btn btn-primary " href="administrativo.php" role="button">Administração &raquo;</a>
         <a class="btn btn-primary " href="fecharAplicacao.php" role="button">Sair [x]</a>
      </div>
    
</div>
    
<div class="jumbotron">
    <div class="container">
      <h2>AGENDA DO DIA</h2>
  <p><?php
  print date("d/m/Y");
  ?>
  </p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Nome Contato</th>
        <th>Assunto</th>
        <th>Hora</th>
        <th>Local</th>
        <th>Vai?</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <tr>
   <?php
   
   $pesquisaAgenda = new controleDados();
   
   $pesquisaAgenda->setDataCompromisso(date("Y-m-d"));
   $retorno = $pesquisaAgenda->pesquisaCompromissoDia(); 
  
  foreach ($retorno as $dadosAgenda){
      $statusAgendaSim = ($dadosAgenda['agenda_status'] == "S")?'<font color=" #FF0000">[SIM]</font>' : 'SIM';
      $statusAgendaNao = ($dadosAgenda['agenda_status'] == "N")?'<font color=" #FF0000">[NÃO]</font>' : 'NÃO';
      ?><td><?= $dadosAgenda['agendamento_nome'];?></td>
        <td><?= $dadosAgenda['agendamento_descricao'];?></td> 
        <td><?= $dadosAgenda['agendamento_hora'];?></td> 
        <td><?= $dadosAgenda['agendamento_titulo'];?></td>
        <td><a href="../controller/atualizaStatus.php?status=s&seqnota=<?= $dadosAgenda['agenda_cod_agendamentos'];?>"><?=$statusAgendaSim?></a> | <a href="../controller/atualizaStatus.php?status=n&seqnota=<?= $dadosAgenda['agenda_cod_agendamentos'];?>"><?=$statusAgendaNao?></a></td> 
        <td><a href="../controller/remover.php?nome=<?= $dadosAgenda['agendamento_nome'];?>">Remover</a></td> 
        <td><a href="atualiza.php?nome=<?= $dadosAgenda['agendamento_nome'];?>">Atualizar   </a></td>
        <td><a href="pesquisa.php?nome=<?= $dadosAgenda['agendamento_nome'];?>">Visualizar</a></td>
      
      </tr> <?php
  }
   
   ?> 
    </tbody>
  </table>
</div> 
    </div>
</div>
  
<!-- Modal Cadastro-->    
<div class="modal fade" id="modalContato" tabindex="-1" role="dialog" aria-labelledby="modalContato" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ADICIONAR NOVO CONTATO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">  
        <form id="envia_msg" action="#" method="POST">
         
            <div class="input-group">
            <label class="col-sm-2 control-label">Nome:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="nome" placeholder="Nome" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Empresa:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="empresa" placeholder="Empresa" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Telefone&nbsp;Comercial</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="telefoneCom" placeholder="Telefone Comercial" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Celular:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="celular" placeholder="Telefone Celular" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Skipe:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="skipe" placeholder="Skipe" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">E&nbsp;-&nbsp;mail:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="email" placeholder="E-mail" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label" for="ex6">Site:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="site" placeholder="Endereço home page" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Cidade:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="cidade" placeholder="Cidade" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Estado:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="estado" placeholder="Estado" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Bairro:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="bairro" placeholder="Bairro" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Endereço:</label>
            <div class="col-sm-12">
            <input id="email" type="text" class="form-control" name="endereco" placeholder="Endereço" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Informações:</label>
            <div class="col-sm-12">
            <textarea name="msg" placeholder="Sua Mensagem..." required></textarea>
            </div>
            </div>
                    
                    
          </div>
         </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="salvar">Inserir Contato</button>  
      </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>        
      </div>
    </div>
  </div>
</div>

<!-- Modal Agendamento -->
<div class="modal fade" id="modalAgendar" tabindex="-1" role="dialog" aria-labelledby="modalAgendar" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">CADASTRO DE COMPROMISSO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">  
        <form id="agendaCompromisso" action="#" method="POST">
         
            <div class="input-group">
            <label class="col-sm-2 control-label">Data&nbsp;Compromisso:</label>
            <div class="col-sm-12">
            <input type='text' name="dataCompromisso" placeholder="Data Compromisso" id='dataCompromisso' class="form-control2">    
            
            </div>
            </div>
                        
            <div class="input-group">
            <label class="col-sm-2 control-label">Hora&nbsp;Compromisso:</label>
            <div class="col-sm-12">
            <input id="hora" type="text" class="form-control" name="hora" placeholder="Hora Compromisso" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
                <label class="col-sm-2 control-label">Título&nbsp;Compromisso:</label>
            <div class="col-sm-12">
            <input id="tipocompromisso" type="text" class="form-control" name="tipocompromisso" placeholder="Título Compromisso" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Local&nbsp;Compromisso:</label>
            <div class="col-sm-12">
            <input id="localcompromisso" type="text" class="form-control" name="localcompromisso" placeholder="Local Compromisso" required autofocus>
            </div>
            </div>
            
            <div class="input-group">
            <label class="col-sm-2 control-label">Vínculo&nbsp;Cliente:</label>
            <div class="col-sm-12">
            <input id="pesquisaCli" type="text" class="form-control" name="vinculocliente" placeholder="Víncular Cliente" required autofocus>
            <div id="retornoConsulta"></div>
            </div>
            </div>
                        
            <div class="input-group">
            <label class="col-sm-2 control-label">Informações:</label>
            <div class="col-sm-12">
            <textarea name="descricaoMsn" placeholder="Sua Mensagem..." required></textarea>
            </div>
            </div>
                    
                    
          </div>
         </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="registroCompromisso">Gravar Compromisso</button>  
      </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>        
      </div>
    </div>
  </div>
</div>
  
  
  <script src="./js/jquery.js"></script>
  <script src="./bootstrap/js/bootstrap.min.js"></script>
  <script src="./js/jquery-ui.js"></script>
  <script type="text/javascript" src="./js/jquery.maskedinput-1.1.1.js" ></script>
  <link href="./css/jquery-ui.css" rel="stylesheet"> 
  <script type="text/javascript" src="./js/padrao.js"></script>
 
    
</body>
</html>

<script>
      
 $(document).ready(function() {
  $('#salvar').click(function(){
		event.preventDefault();
		var formDados = $('#envia_msg').serialize();

		$.ajax({                    
			url:'gravarContato.php',
			type:'POST',                        
			data:formDados,			
			success:function (data){  
                         
                         if(data == 0){  
                            alert("Preencha todos os campos do Contato");                            
                         } else {
                            alert("Dados gravados");
                            $('#divCarrega').html(data);
                            $('#modalContato').modal('hide'); // Fechar a modal
                            $('form').reset(); // Limpar os campos do form
                         }  
			 
	            }
	    });			
	});
});

$(document).ready(function(){

        $('#pesquisaCli').keyup(function(){ // Pesquisa dados sem refresh

            $('form').submit(function(){
                var dados = $(this).serialize();

                $.ajax({
                    url: 'pesq_cliente.php',
                    method: 'post',
                    dataType: 'html',
                    data: dados,
                    success: function(data){
                        $('#retornoConsulta').empty().html(data);
                    }
                });

                return false;
            });

            $('form').trigger('submit');

        });
    });

     
 $(document).ready(function() {
  $('#registroCompromisso').click(function(){
		event.preventDefault();
		var formDadosComp = $('#agendaCompromisso').serialize();

		$.ajax({                    
			url:'gravarCompromisso.php',
			type:'POST',                        
			data:formDadosComp,			
			success:function (data){  
                             
                            alert("Dados gravados");
                            location.reload(); // Atualiza a página principal
                            $('#divCarrega').html(data);
                            $('#modalContato').modal('hide');
                            
                       
	            }
	    });			
	});
});   
   
   
   $( function() {
    $( "#dataCompromisso" ).datepicker({
     dateFormat: "dd/mm/yy",
     dayNames: [ "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
     monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
     dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"]     
        });
    
  } );
   
  </script>
