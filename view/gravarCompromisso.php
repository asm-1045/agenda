<?php
namespace view;
use controller\controleDados;

include_once '../controller/controleDados.php';

$gravarCompromisso = new controleDados();

if (empty($_POST['dataCompromisso'])):
   
   return 0; 
endif;
if (empty($_POST['hora'])):
    
   return 0; 
endif;
if (empty($_POST['tipocompromisso'])):
    
   return 0; 
endif;
if (empty($_POST['localcompromisso'])):
    
   return 0; 
endif;


$dataAtual = explode('/',$_POST['dataCompromisso']);

$gravarCompromisso->setDataCompromisso($dataAtual[2].'-'.$dataAtual[1].'-'.$dataAtual[0]);
$gravarCompromisso->setHora($_POST['hora']);
$gravarCompromisso->setTipocompromisso($_POST['tipocompromisso']);
$gravarCompromisso->setLocalcompromisso($_POST['localcompromisso']);
$gravarCompromisso->setVinculocliente($_POST['vinculocliente']);
$gravarCompromisso->setDescricaoMsn($_POST['descricaoMsn']);

$retornoInfo = $gravarCompromisso->inserirCompromisso();
$gravarCompromisso->setcodigo($retornoInfo);
$retornoInfo = $gravarCompromisso->registroAgenda();

return 1;

