<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Acesso Agenda</title>

    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">    
    <link href="./css/padraoCss.css" rel="stylesheet">

  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="../controller/validaAcesso.php" method="POST">
        <h2 class="form-signin-heading">Dados de Acesso</h2>
        <label for="usuario" class="sr-only">Usuário</label>
        <input type="usuario" id="inputUsuario" name="inputUsuario" class="form-control" placeholder="Usuário" required autofocus>
        <label for="senha" class="sr-only">Senha</label>
        <input type="password" id="inputSenha" name="inputSenha" class="form-control" placeholder="Senha" required>
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Acessar</button>
      </form>

    </div> 
    <?php
    if(!empty($_GET['error'])):
        
        if($_GET['error']== 1):
           print "<center>Preencher todos os campos</center>";
        elseif($_GET['error']== 2):
           print "<center>Usuário ou senha invalido</center>";
        else:
           print "<center>Error 404</center>"; 
        endif;
        
    endif;
    
    ?>
      
  </body>
</html>
 

