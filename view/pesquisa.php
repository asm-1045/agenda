<?php
namespace view;
use controller\controleDados;

include_once '../controller/controleDados.php';
date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>Agenda Eletrônica</title>    
  <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="./css/estilos.css" rel="stylesheet">
  
  
</head>
<body>
   
<div class="jumbotron">
      <div class="container">
         <h1>Agenda Telefônica</h1>         
          <a class="btn btn-primary" href="agenda.php" role="button">Home &raquo;</a>
          <a class="btn btn-primary" href="pesquisa.php" role="button">Pesquisa &raquo;</a>
      </div>
    
</div>
    
<div class="jumbotron">
    <div class="container">
      <h2>Pesquisa Agendamento</h2>
         
<form method="GET" action="" name="Cadastro">

       <input type='text' name='dataPesq' size='20' maxlength='20' id='datepicker'>
       <input name="pesquisa" value="Pesquisar" type="submit" >
       <br>
</form>       
       <table class="table table-striped">
    <thead>
      <tr>
        <th>Nome Contato</th>
        <th>Assunto</th>
        <th>Hora</th>
        <th>Local</th>
        <th>Vai?</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <tr>
   <?php
   
   $pesquisaAgenda = new controleDados();
   
   if (!empty($_GET['dataPesq'])):
     $quebraData = explode("/",$_GET['dataPesq']);
     $newData = $quebraData[2]."-".$quebraData[1]."-".$quebraData[0];
   $pesquisaAgenda->setDataCompromisso($newData);
   endif;
   $retorno = $pesquisaAgenda->pesquisaCompromissoDia();
  
  foreach ($retorno as $dadosAgenda){
      ?><td><?= $dadosAgenda['agendamento_nome'];?></td>
        <td><?= $dadosAgenda['agendamento_descricao'];?></td> 
        <td><?= $dadosAgenda['agendamento_hora'];?></td> 
        <td><?= $dadosAgenda['agendamento_titulo'];?></td>
        <td><a href="atualizaStatus.php?status=s">SIM</a> | <a href="atualizaStatus.php?status=n">NÃO</a></td> 
        <td><a href="../controller/remover.php?nome=<?= $dadosAgenda['agendamento_nome'];?>">Remover</a></td> 
        <td><a href="atualiza.php?nome=<?= $dadosAgenda['agendamento_nome'];?>" >Atualizar</a></td>
        <td><a href="pesquisa.php?nome=<?= $dadosAgenda['agendamento_nome'];?>">Visualizar</a></td>
      
      </tr> <?php
  }
   
   ?> 
    </tbody>
  </table>
       
    </div>
</div>
    
    
    <?php
    if (!empty($_GET['nome'])){
        
    ?>
    
    <div class="jumbotron">
      <div class="container">
         <h1>Informações</h1>         
         
         <table border='1'><tr><th>DATA</th><th>HORA</th><th>CONTATO</th><th>EMPRESA</th><th>TEL. COMERCIAL</th>
         <th>TEL. CELULAR</th><th>SKYPE</th><th>E-MAIL</th><th>SITE</th><th>UF</th><th>MUNICÍPIO</th><th>BAIRRO</th>
         <th>ENDEREÇO</th></tr><tr>
    <?php     
       
        $pesquisaAgenda->setNome($_GET['nome']);    
        $retorno = $pesquisaAgenda->infoAgendamento();
  
        foreach ($retorno as $dadosAgenda){ 
            ?><td><?=$dadosAgenda['data_agenda']?></td>
              <td><?=$dadosAgenda['agendamento_hora']?></td>
              <td><?=$dadosAgenda['agendamento_nome']?></td>
              <td><?=$dadosAgenda['cliente_empresa']?></td>
              <td><?=$dadosAgenda['cliente_telefone']?></td>
              
              <td><?=$dadosAgenda['cliente_celular']?></td>
              <td><?=$dadosAgenda['cliente_skype']?></td>
              <td><?=$dadosAgenda['cliente_email']?></td>
              <td><?=$dadosAgenda['cliente_site']?></td>
              <td><?=$dadosAgenda['cliente_estado']?></td>
              <td><?=$dadosAgenda['cliente_cidade']?></td>
              <td><?=$dadosAgenda['cliente_bairro']?></td>
              <td><?=$dadosAgenda['cliente_endereco']?></td>
     <?php           
        }        
                 
    ?>     
             </tr></table>      
              
      </div>
    
</div>
    
    
        
    <?php    
    }
    
    ?>
    
    
    
    <script src="./js/jquery.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script> 
    <script src="./js/jquery-ui.js"></script>
    <script src="./js/Chart.min.js"></script>
    <script type="text/javascript" src="./js/jquery.maskedinput-1.1.1.js" ></script>
    <link href="./css/jquery-ui.css" rel="stylesheet">    
    <script type="text/javascript" src="./js/padrao.js" ></script>
 
</body>
</html>

